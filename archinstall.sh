#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
hwclock --systohc
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:123 | chpasswd

# Remove the tlp package if you are installing on a desktop or vm
pacman -S grub efibootmgr networkmanager network-manager-applet wpa_supplicant mtools dosfstools base-devel linux-headers avahi xdg-user-dirs xdg-utils nfs-utils inetutils dnsutils bluez bluez-utils alsa-utils wireplumber pipewire pipewire-alsa pipewire-pulse pipewire-jack openssh rsync reflector acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld sof-firmware nss-mdns acpid

# pacman -S xf86-video-amdgpu
# pacman -S nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable sshd
systemctl enable avahi-daemon
# Comment this command out if you didn't install tlp
systemctl enable tlp 
# Updates your mirrorlist weekly
systemctl enable reflector.timer 
# Improves ssd performance
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable acpid

useradd -m azzpg7
echo azzpg7:123 | chpasswd
usermod -aG libvirt azzpg7

echo "azzpg7 ALL=(ALL) ALL" >> /etc/sudoers.d/ermanno

# Disable Wake-On-LAN
echo "ACTION==\"add\", SUBSYSTEM==\"net\", NAME==\"enp*\", RUN+=\"/usr/bin/ethtool -s $name wol g\"" >> /etc/udev/rules.d/81-wol.rules

printf "\e[1;32mDone! Type exit, umount -R /mnt and reboot.\e[0m\n"
