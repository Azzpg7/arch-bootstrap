# turn on parallel downloading from pacman
sudo sed -i 's/#Parallel/Parallel/g' /etc/pacman.conf

# installing paru
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd ..
sudo rm -R paru

# installing ly display manager
paru -S ly
sudo systemctl enable ly.service

# installing base i3 packages
sudo pacman -S xorg i3 dmenu nitrogen alacritty picom firefox unzip zsh telegram-desktop keepassxc flameshot dex xss-lock brightnessctl i3status-rust neovim bat dbus dunst thunar exa fzf inkscape lua-language-server neofetch pango pulsemixer speedtest-cli tree valgrind wget wireguard-tools xclip zathura ripgrep clang python-pip pamixer mesa
paru -S gotop lf battop profile-sync-daemon
sudo echo "azzpg7 ALL=(ALL) NOPASSWD: /usr/bin/psd-overlay-helper" >> /etc/sudoers
systemctl --user enable psd

pip install pynvim python-lsp-server mypy flake8

# installing patched fonts with icons
sudo mkdir /usr/share/fonts/nerd-fonts
cd /usr/share/fonts/nerd-fonts
sudo curl -LJO https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip
sudo unzip Hack.zip
sudo rm Hack.zip
fc-cache -vf

# downloading my config files
cd ~
git clone https://gitlab.com/azzpg7/dotfiles
cp -r ~/dotfiles/.config/alacritty/ ~/.config/
cp -r ~/dotfiles/.config/i3/ ~/.config/
cp -r ~/dotfiles/.config/i3status-rust/ ~/.config/
cp -r ~/dotfiles/.config/lf/ ~/.config/
cp -r ~/dotfiles/.config/nvim/ ~/.config/
cp ~/dotfiles/.zshrc ~/
cp -r ~/dotfiles/.config/zsh/ ~/.config/
cp ~/dotfiles/Pictures/background.jpeg ~/Pictures/

chsh -s /bin/zsh
